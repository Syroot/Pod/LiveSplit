state("podx3dfx", "1997-10-25 22:37:51") // 2.2.8.1 / 2.2.8.0, 3dfx, GS1, MMX
{
	bool finished : 0x137104;
	byte lapPart : 0x13CD90; // lo=lap hi=part
	uint totalLapTime : 0x13CD98;
	uint totalPartTime : 0x13CD9C;
	uint raceTime : 0x13D388;
	int screen : 0x13DB54;
	uint chshpTime : 0x1CEDF0;
	int raceState : 0x1EAC84; // 0=drive 1=countdown 2=go
	uint gameMode : 0x2A91A8; // bits, 0=single 1=chshp 2=ta 9=taFree
}
state("podx3dfx", "1998-11-12 22:53:27") // 2.2.9.0 / 2.2.9.0, 3dfx, GS2, MMX
{
	bool finished : 0x1211B4;
	byte lapPart : 0x126E40;
	uint totalLapTime : 0x126E48;
	uint totalPartTime : 0x126E4C;
	uint raceTime : 0x127438;
	int screen : 0x1281A8;
	uint chshpTime : 0x1BA520;
	int raceState : 0x1D6958;
	uint gameMode : 0x2970D0;
}

// ---- Script Actions ----

startup
{
	// Create settings.
	settings.Add("splitLap", true, "Split Laps");
	settings.Add("splitPart", false, "Split Parts");

	// Set to display game time.
	timer.CurrentTimingMethod = TimingMethod.GameTime;
}

init
{
	// Determine version from PE TimeDateStamp.
	IntPtr baseAddress = modules.First().BaseAddress;
	int peOffset = memory.ReadValue<int>(baseAddress + 0x3C);
	int timeDateStamp = memory.ReadValue<int>(baseAddress + peOffset + 0x8);
	switch (timeDateStamp)
	{
		case 0x34632433: version = "1997-10-25 22:37:51"; break;
		case 0x364B66E7: version = "1998-11-12 22:53:27"; break;
		default: throw new NotImplementedException("Unknown PE TimeDateStamp 0x" + timeDateStamp.ToString("X8"));
	}

	// Create global functions.
	vars.ConvertTime = (Func<uint, TimeSpan>)(time =>
	{
		// Game reimplementation to prevent inaccuracy of TimeSpan.FromSeconds(time / (double)(1 << 16)).
		double part = time;
		int mins = (int)(part / (60.0 * 65536));

		part -= mins * 60.0 * 65536;
		int secs = (int)(part * (1.0 / 65536));

		part -= secs * 65536;
		int cens = (int)(part / (65536 / 100.0) + 0.5);

		if (cens > 99)
		{
			secs += cens - 99;
			cens -= 100;
		}
		if (secs > 59)
		{
			mins += secs - 59;
			secs -= 60;
		}

		return new TimeSpan(0, 0, mins, secs, cens * 10);
	});
	vars.GetGameMode = (Func<uint, string>)(gameMode =>
	{
		if ((gameMode & 1 << 0) != 0) return "single";
		if ((gameMode & 1 << 1) != 0) return "chshp";
		if ((gameMode & 1 << 9) != 0) return "taFree";
		if ((gameMode & 1 << 2) != 0) return "ta";
		return "";
	});

	// Create global variables.
	vars.inRace = true;
	vars.gameTime = 0u;
	vars.split = false;
}

// ---- Timer Actions ----

update
{
	string gameMode = vars.GetGameMode(current.gameMode);

	// Determine when to add proper race times.
	bool inRaceScreen = current.screen == -1 || current.screen == 32;
	if (!vars.inRace)
		vars.inRace = inRaceScreen && (gameMode.StartsWith("ta") || current.raceState > 0);
	if (!inRaceScreen)
		vars.inRace = false;

	vars.gameTime = 0u;

	// Add total championship time.
	if (gameMode == "chshp")
		vars.gameTime = current.chshpTime;

	// Add current split or race time.
	bool allowSplit = old.raceTime != current.raceTime;
	if (allowSplit && settings["splitPart"] && old.totalPartTime < current.totalPartTime)
	{
		vars.gameTime += current.totalPartTime;
		vars.split = true;
	}
	else if (allowSplit && settings["splitLap"] && old.totalLapTime < current.totalLapTime)
	{
		vars.gameTime += current.totalLapTime;
		vars.split = true;
	}
	else if (vars.inRace && current.raceState == 0)
	{
		int currentLap = current.lapPart & 0xF;
		if (current.finished)
			vars.gameTime += current.totalLapTime;
		else if (!gameMode.StartsWith("ta") || currentLap > 0)
			vars.gameTime += current.raceTime;
	}
}

isLoading
{
	// Always use game time.
	return true;
}

gameTime
{
	// Game time as calculated by update.
	return vars.ConvertTime(vars.gameTime);
}

reset
{
	switch ((int)current.screen)
	{
		case 17: // track select
		case 18: // time attack menu
		case 40: // championship menu
			return true;
	}
	return false;
}

split
{
	// Split once as calculated by update.
	bool split = vars.split;
	vars.split = false;
	return split;
}

start
{
	// Start once race is properly loaded.
	return vars.inRace;
}
