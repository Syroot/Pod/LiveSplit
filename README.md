# LiveSplit

Utilities supporting Pod in LiveSplit.

## Auto Splitters

Available directly in LiveSplit once selecting POD as the game in the Splits Editor.

- [`pod.asl`](https://gitlab.com/Syroot/Pod/LiveSplit/-/raw/main/pod.asl) - uses in-game timer, auto-start, auto-reset, all mode support, optional lap and part splits

## [Layouts](layouts)

Right-click and copy the URL of one of the following layouts to open them in LiveSplit.

- [`pod_chshp.lsl`](https://gitlab.com/Syroot/Pod/LiveSplit/-/raw/main/layouts/pod_chshp.lsl) - basic subsplit layout for championship comparisons
- [`pod_ta.lsl`](https://gitlab.com/Syroot/Pod/LiveSplit/-/raw/main/layouts/pod_ta.lsl) - basic subsplit layout for time attack comparisons

## [Splits](splits)

Right-click and copy the URL of one of the following splits to open them in LiveSplit.

- [`pod_chshp.lss`](https://gitlab.com/Syroot/Pod/LiveSplit/-/raw/main/splits/pod_chshp.lss) - 16 retail tracks championship with lap subsplits
- [`pod_chshp_gold.lss`](https://gitlab.com/Syroot/Pod/LiveSplit/-/raw/main/splits/pod_chshp_gold.lss) - 16 gold tracks championship with lap subsplits
- [`pod_ta_2p.lss`](https://gitlab.com/Syroot/Pod/LiveSplit/-/raw/main/splits/pod_ta_2p.lss) - time attack with 2 lap part subsplits
- [`pod_ta_3p.lss`](https://gitlab.com/Syroot/Pod/LiveSplit/-/raw/main/splits/pod_ta_3p.lss) - time attack with 3 lap part subsplits
- [`pod_ta_4p.lss`](https://gitlab.com/Syroot/Pod/LiveSplit/-/raw/main/splits/pod_ta_4p.lss) - time attack with 4 lap part subsplits
